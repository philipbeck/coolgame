const HEIGHT = 600;
const WIDTH = 800;
//game constants
//colours
const BACKGROUND = "#911b89";//"#ff0caa" was just too bad for my eyes
const PLAYER_COLOR = "#42f474";
const BULLET_COLOR = "#e2f442";
const ENEMY_COLOR = "#2857ff";
//others
const RUN_SPEED = 50;
//physics constants
const ACCELERATION = 0.3;

//game variables
var running = true;
var game = new Game();
//var player = new Player(50,50,0.0001,0.0001,0,0,10,20);
var bullets = [];

//key varibles
var leftKey = false;
var rightKey = false;
var upKey = false;
var downKey = false;
var spaceKey = false;

var screen = document.getElementById('screen');

function getPixels(){
  screen.height = HEIGHT;
  screen.width = WIDTH;
}

//function to totally redraw the screen man!
function redraw(){
  if(screen.getContext){
    let context = screen.getContext("2d");
    //clear background first
    context.fillStyle = BACKGROUND;
    context.fillRect(0, 0, WIDTH, HEIGHT);
    //draw player second
    context.fillStyle = PLAYER_COLOR;
    context.beginPath();
    context.arc(game.player.x,game.player.y,game.player.size,0,2*Math.PI);
    context.fill();

    //draw bullets
    context.fillStyle = BULLET_COLOR;
    for(let i = 0; i < game.bullets.length; i++){
      let b = game.bullets[i];
      context.beginPath();
      context.arc(b.x,b.y,b.size,0,2*Math.PI);
      context.fill();
    }
    context.fillStyle = ENEMY_COLOR;
    for(let i = 0; i < game.enemies.length; i++){
      let e = game.enemies[i];
      context.beginPath();
      context.arc(e.x,e.y,e.size,0,2*Math.PI);
      context.fill();
    }
  }
}

//stuff for keyboard input
window.onkeydown = function(e){
  let key = e.keyCode;
  console.log(key);
  switch(key){
    case 38://up arrow
    case 87://w
      upKey = true;
      break;
    case 40://down arrow
    case 83://s
      downKey = true;
      break;
    case 37://left arrow
    case 65://a
      leftKey = true;
      break;
    case 39://right arrow
    case 68://d
      rightKey = true;
      break;
    case 32:
      spaceKey = true;
      break;
    default:
      break;
  }
}

window.onkeyup = function(e){
  let key = e.keyCode;
  switch(key){
    case 38://up arrow
    case 87://w
      upKey = false;
      break;
    case 40://down arrow
    case 83://s
      downKey = false;
      break;
    case 37://left arrow
    case 65://a
      leftKey = false;
      break;
    case 39://right arrow
    case 68://d
      rightKey = false;
      break;
    default:
      break;
  }
}

function step(){
  if(leftKey){
    game.player.accX -= ACCELERATION;
  }
  if(rightKey){
    game.player.accX += ACCELERATION;
  }
  if(upKey){
    game.player.accY -= ACCELERATION;
  }
  if(downKey){
    game.player.accY += ACCELERATION;
  }
  if(spaceKey){
    game.bullets.push(game.player.shoot());
    spaceKey = false;
  }
  game.step();
}

function run(){
  setTimeout(function () {
    step();
    redraw();
    if(running){
      run();
    }
  }, RUN_SPEED);
}

//MAIN GAME STUFF!!!!!!!!!!!!!!!!!!!!!!!
getPixels();
redraw();
run();

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
