//some constants
const DRAG = 0.4;
const BULLET_SIZE = 2;
const BULLET_SPEED = 15;
const BULLET_MASS = 3;
const MAX_BOUNCE = 10;
const MAX_VEL = 20;
const GRAVITY = 5;//how far away gravity starts getting weaker <-- not really right
const ENEMY_AGE = 250;
const MAX_ENEMY_SIZE = 15;
const MIN_ENEMY_SIZE = 7;
const MAX_ENEMY_ACC = 3;
//CLASSES FOR MONSTERS ETC
class Entity{
  constructor(x, y, velX, velY, accX, accY, size, mass){
    this.x = x;
    this.y = y;
    this.velX = velX;
    this.velY = velY;
    this.accX = accX;
    this.accY = accY;
    this.size = size;
    this.mass = mass;
    this.bounces = 0;
    this.age = 0;
  }

  step(){
    //update position
    this.x += this.velX;
    if(this.x > WIDTH){
      this.x = WIDTH;
      this.velX = -this.velX;
      this.bounces++;
    }
    else if(this.x < 0){
      this.x = 0;
      this.velX = -this.velX;
      this.bounces++;
    }
    this.y += this.velY;
    if(this.y > HEIGHT){
      this.y = HEIGHT;
      this.velY = -this.velY;
      this.bounces++;
    }
    else if(this.y < 0){
      this.y = 0;
      this.velY = -this.velY;
      this.bounces++;
    }
    //update velocity
    this.velX += this.accX;
    this.velY += this.accY;
    if(this.velX > MAX_VEL){
      this.velX = MAX_VEL;
    }
    else if(this.velX < -MAX_VEL) {
      this.velX = -MAX_VEL;
    }
    if(this.velY > MAX_VEL){
      this.velY = MAX_VEL;
    }
    else if(this.velY < -MAX_VEL) {
      this.velY = -MAX_VEL;
    }
    //update acceleration
    this.accX *= DRAG;
    this.accY *= DRAG;

    this.age++;
  }

  crash(entity){
  }
}

//player class
class Player extends Entity{
  shoot(){
    let hype = Math.sqrt((this.velX*this.velX) + (this.velY*this.velY));
    let cos = this.velX/hype;
    let sin = this.velY/hype;
    let velX = -cos * BULLET_SPEED;
    let velY = -sin * BULLET_SPEED;
    let b = new bullet(this.x - cos*(this.size + BULLET_SIZE), this.y - sin*(this.size + BULLET_SIZE),
    velX, velY, 0, 0, BULLET_SIZE, BULLET_MASS);
    this.velX -= velX * BULLET_MASS/this.mass;
    this.velY -= velY * BULLET_MASS/this.mass;
    return b;
  }
}

class Enemy extends Entity{
  //enemy business goes in here
  step(player){
    super.step();
    let x = player.x - this.x;
    let y = player.y - this.y;
    //lazy way around pythagoras, makes a diamond rather than a square but whatevs
    let dist = (Math.abs(x) + Math.abs(y));
    if(this.accX < MAX_ENEMY_ACC){
      this.accX += (x/dist);
    }
    if(this.accY < MAX_ENEMY_ACC){
      this.accY += (y/dist);
    }
    if(this.age > ENEMY_AGE){
      this.size++;
      this.age = 0;
      if(this.size === MAX_ENEMY_SIZE){
        this.size = MIN_ENEMY_SIZE;
        let e = new Enemy(this.x - this.size, this.y - this.size, this.velX, this.velY, this.accX, this.accY, MIN_ENEMY_SIZE, this.mass);
        this.x += this.size;
        this.y += this.size;
        return e;
      }
    }
  }
}

class bullet extends Entity{
  //there will probably be some stuff in here
}

//will one day be an object to store all the game stuff
class Game{
  constructor(){
    this.player = new Player(WIDTH/2,HEIGHT/2,0.0001,0.0001,0,0,10,20);
    this.bullets = [];
    this.enemies = [];
    for(let i = 0; i < 5; i++){
      this.enemies.push(new Enemy(Math.random() * 800, Math.random() * 600, 0,0,0,0,7,5));
    }
  }

  step(){
    this.player.step();
    //step bullets
    for(let i = 0; i < this.bullets.length; i++){
      let b = this.bullets[i];
      b.step();
      if(b.bounces > MAX_BOUNCE){
        this.bullets.splice(i, 1);
        i--;
      }
    }

    //step enemies
    for(let i = 0; i < this.enemies.length; i++){
      let e  = this.enemies[i].step(this.player);
      if(e != null){
        this.enemies.push(e);
      }
    }

    this.getCollisions();
    this.bulletGravity();
  }

  //bullets have gravitational pull for top bants
  bulletGravity(){
    for(let i = 0; i < this.bullets.length; i++){
      let b = this.bullets[i];
      let pull = getGravity(this.player, b);
      this.player.accX += pull[0];
      this.player.accY += pull[1];
      for(let j = 0; j < this.enemies.length; j++){
        let e = this.enemies[j];
        pull = getGravity(e, b);
        e.accX += pull[0];
        e.accY += pull[1];
        b.accX += pull[2];
        b.accy += pull[3];
      }
    }
    //for player
    for(let j = 0; j < this.enemies.length; j++){
      let e = this.enemies[j];
      let pull = getGravity(e, this.player);
      e.accX += pull[0];
      e.accY += pull[1];
      this.player.accX += pull[2];
      this.player.accY += pull[3];
    }
  }

  //this function will deal with collisions (starting with just the bullets and enemies)
  getCollisions(){
    for(let i = 0; i < this.bullets.length; i++){
      let b = this.bullets[i];
      for(let j = 0; j < this.enemies.length; j++){
        let e = this.enemies[j];
        if(getCollision(b, e)){
          e.size--;
          e.velY += b.velY * (b.mass/e.mass);
          e.velX += b.velX * (b.mass/e.mass);
          //get rid of bullet
          this.bullets.splice(i,1);
          i--;
        }
      }
    }
  }
}

//horrible function for gravitational pull
function getGravity(e1, e2){
  let x = e2.x-e1.x;
  let y = e2.y-e1.y;
  let dist = Math.sqrt((x*x) + (y*y));
  if(dist > (e1.size + e2.size)){
    let cos = x/dist;
    let sin = y/dist;
    let pull1 = (GRAVITY/(dist - e1.size - e2.size + 1)) * (e2.mass/e1.mass);
    let pull2 = (GRAVITY/(dist - e1.size - e2.size + 1)) * (e1.mass/e2.mass);
    return [pull1*cos, pull1*sin, pull2*cos, pull2*sin];
  }
  else{
    return [0,0,0,0];
  }
}

function getCollision(e1, e2){
  let distanceSquared = Math.pow(e1.x-e2.x,2) + Math.pow(e1.y-e2.y, 2);
  let minDist = Math.pow(e1.size + e2.size, 2);
  if(distanceSquared < minDist){
    return true;
  }
  else{
    return false;
  }
}
